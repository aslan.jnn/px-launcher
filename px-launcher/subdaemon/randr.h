#pragma once

#include <xcb/xcb.h>
#include <utility>
#include <memory>

namespace subdaemon {
    class RandRSetting {
    public:
        RandRSetting();
        ~RandRSetting();
        RandRSetting(const RandRSetting&) = delete;
        RandRSetting& operator=(const RandRSetting&) = delete;

        void update(xcb_connection_t* conn, xcb_screen_t* scr);
        void resize_display(std::pair<uint16_t, uint16_t> sz);

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}
