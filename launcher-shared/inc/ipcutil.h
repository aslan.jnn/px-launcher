#pragma once

#include <string>
#include "shared/xcomponent.h"

namespace ipcshared {
    const int api_revision = 1;

    const std::string ipc_basedir = "/run/px-launcher/";

    std::string get_ipc_server_addr(px::xutil::XDisplayComponent& display);
    std::string get_ipc_reply_addr(uint16_t reply_id);
}

