#pragma once

#include <unistd.h>
#include <string>
#include <vector>
#include <memory>

namespace raii {
    class Process {
    public:
        /**
         * Set for the destructor to either wait for the process to exit or simply ignore it.
         */
        bool sync_on_exit = true;
        /**
         * If set, this instance's destructor will kill the associated child process according to the set signal.
         * If sync_on_exit is true, the destructor will also waits for the child process to close.
         */
        int signal_on_exit = 0;

        Process();
        Process(pid_t existing);
        /**
         * Fork and exec a new process.
         * @param argv Arguments to be passed to the new executable. argv[0] is the executable to start.
         * @param env Environment variable to set for the new process.
         */
        Process(std::vector<std::string> argv, std::vector<std::pair<std::string, std::string>> env);
        /**
         * Returns PID stored in this object.
         */
        Process(const Process&) = delete;
        ~Process();
        Process& operator=(const Process&) = delete;
        pid_t get_pid();
        /**
         * Wait until children PID exits. This instance will be invalidated after wait finishes.
         */
        void wait();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}