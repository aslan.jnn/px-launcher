#include "client.h"
#include "shared/i18n.h"
#include "shared/nulldelimiterbuilder.h"
#include "ipcclient.h"
#include <sys/wait.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <vector>

using namespace std;
using namespace px;
using namespace px::xutil;
using namespace px::xutil;
using namespace ipcshared;

px::xutil::XDisplayComponent pxlnclient::current_frontend_dpy() {
    XDisplayComponent fedpy {""};
    auto env_display = getenv("DISPLAY");
    if(!env_display) {
        puts(_("DISPLAY environment variable not set."));
    } else {
        fedpy = XDisplayComponent{env_display};
    }
    if(fedpy.is_invalid()) {
        puts(_("Invalid DISPLAY environment variable."));
    }
    return fedpy;
}

void pxlnclient::ask_reload() {
    XDisplayComponent fedpy = current_frontend_dpy();
    if(fedpy.is_invalid()) {
        return;
    }
    IPCClient ic(fedpy);
    //request
    msg::MessageStore req;
    auto reqhdr = new(req.header.data) msgreq::RequestHeader(msgreq::RequestType::RELOAD_CFG);
    auto rep = ic.request_for_reply(req);
    auto rephdr = (msgrep::ReloadCfgReply*)rep.second.header.data;
    if(rep.first == IPCClient::ReplyStatus::SUCCESS && rephdr->success) {
        puts(_("Configuration reloaded successfully."));
    } else {
        puts(_("Configuration reload failed. Make sure that daemon is running, the configuration file is valid and number of groups doesn't change in the configuration file."));
    }
}

void pxlnclient::exec(int argc, char **argv) {
    XDisplayComponent fedpy = current_frontend_dpy();
    if(fedpy.is_invalid()) {
        return;
    }
    IPCClient ic(fedpy);
    //request
    msg::MessageStore req;
    auto reqhdr = new (req.header.data) msgreq::AskDisplayRequest();
    reqhdr->pid = 0;
    reqhdr->requestor_type = msgreq::DisplayRequestorType::ONDEMAND;
    //response
    auto rep = ic.request_for_reply(req);
    //make sure we still process if server fails (ensure the target program execute normally)
    if(rep.first == IPCClient::ReplyStatus::SUCCESS) {
        //parse header
        auto rephdr = (msgrep::AskDisplayReply*)rep.second.header.data;
        //setenv
        //ensure that: server suggested change, payload has contents, payload is null terminated
        if(rephdr->change && rep.second.payload.size() && rep.second.payload[rep.second.payload.size()-1] == 0) {
            //additional checking should be performed
            setenv("DISPLAY", (char*)&rep.second.payload[0], 1);
        }
    } else {
        puts(_("IPC Client error."));
    }

    //exec
    pid_t pid = fork();
    if(pid) {
        //parent
        //we have to wait, just as if we were running the given command regularly
        waitpid(pid, nullptr, 0);
    } else {
        //child
        //TODO: reset euid
        pid_t rgid = getgid();
        pid_t ruid = getuid();
        if(rgid != getegid()) {
            setegid(rgid);
        }
        if(ruid != geteuid()) {
            seteuid(ruid);
        }
        //exec
        vector<char*> argv_vp;
        argv_vp.insert(argv_vp.begin(), argc, nullptr);
        for(int i=1; i<argc; ++i) {
            argv_vp[i - 1] = argv[i];
        }
        int exec_res = execvp(argv[1], &argv_vp[0]);
        if(exec_res < 0) {
            perror("EXEC");
        }
        return;
    }
}