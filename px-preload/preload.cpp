#include "preload.h"
#include <unistd.h>
#include <dlfcn.h>
#include <cstdio>
#include <cstdlib>
#include "shared/xcomponent.h"
#include "ipcclient.h"
#include "shared/nulldelimiterbuilder.h"
#include "log.h"
#include <cstring>
#include "ipcutil.h"
#include "shared/util.h"

using namespace ipcshared;
using namespace px;
using namespace px::xutil;

typedef xcb_connection_t *(*sig_xcb_connect)(const char *displayname, int *screenp);

bool no_dpy_redir = false;

void pxpr_request_unredirect() {
    no_dpy_redir = true;
}

uint32_t pxpr_system_api_version() {
    return ipcshared::api_revision;
}

xcb_connection_t *xcb_connect(const char *displayname, int *screenp) {
    static Log l;
    //original xcb_connect
    sig_xcb_connect xcb_connect_orig = (sig_xcb_connect)dlsym(RTLD_NEXT, "xcb_connect");
    //before redirection, make sure we're not asked to perform redirection and DISPLAY has a value.
    auto targetdpy = displayname ? displayname : getenv("DISPLAY");
    if(!targetdpy || no_dpy_redir) {
        return xcb_connect_orig(displayname, screenp);
    }
    //ask server if we should replace targetdpy
    XDisplayComponent fedpy {targetdpy};
    if(fedpy.is_invalid()) {
        return xcb_connect_orig(displayname, screenp);
    }
    IPCClient ic(fedpy);
    //request
    msg::MessageStore req;
    auto reqhdr = new (req.header.data) msgreq::AskDisplayRequest();
    reqhdr->pid = l.pid;
    reqhdr->requestor_type = msgreq::DisplayRequestorType::PRELOADER;
    //response
    auto rep = ic.request_for_reply(req);
    if(rep.first == IPCClient::ReplyStatus::SUCCESS) {
        auto rephdr = (msgrep::AskDisplayReply*)rep.second.header.data;
        if(rephdr->change && rep.second.payload.size() && rep.second.payload[rep.second.payload.size()-1] == 0) {
            //change invoke with new parameter
            auto newdpy = (char*)&rep.second.payload[0];
            return xcb_connect_orig(newdpy, screenp);
        }
    } else {
        l.append_log(targetdpy, px::util::string_printf("IPCClient: %d", rep.first));
    }
    //no change
    return xcb_connect_orig(displayname, screenp);
}