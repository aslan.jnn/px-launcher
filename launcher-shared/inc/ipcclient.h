#pragma once

#include "ipcmsg.h"
#include "shared/xcomponent.h"
#include <memory>

namespace ipcshared {
    class IPCClient {
    public:
        enum class ReplyStatus : uint8_t {SUCCESS, REPLY_SOCKET_FAIL, SEND_SOCKET_FAIL, RECV_FAIL, DATA_TRUNCATED, TIMED_OUT};
        IPCClient(px::xutil::XDisplayComponent& server);
        IPCClient(const IPCClient&) = delete;
        IPCClient& operator=(const IPCClient&) = delete;
        ~IPCClient();
        std::pair<ReplyStatus, msg::MessageStore> request_for_reply(msg::MessageStore req);

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}