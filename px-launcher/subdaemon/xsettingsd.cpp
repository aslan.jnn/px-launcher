#include "xsettingsd.h"
#include <shared/memory.h>
#include <cstring>
#include <assert.h>
#include <glog/logging.h>
#include <xcb/xcb_util.h>

using namespace px::memory;
using namespace subdaemon;
using namespace std;

namespace priv {
    pair<uint32_t,unique_ptr<uint8_t,free_delete>> build_data(map<string, XSettingItem> &xsettings);
    xcb_atom_t p_intern_atom(xcb_connection_t* conn, string str);
}

XSettingsDaemon::XSettingsDaemon() {
    //no need for instance for now
}

XSettingsDaemon::~XSettingsDaemon() {}

void XSettingsDaemon::create_xsettings_window(xcb_connection_t *conn, xcb_screen_t *scr, map<string, XSettingItem> &xsettings) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    //mostly a static method as of now + no need to cleanup owner window
    //create window
    xcb_window_t wid = xcb_generate_id(conn);
    err.pop() = xcb_request_check(conn, xcb_create_window_checked(conn, XCB_COPY_FROM_PARENT, wid, scr->root, 1, 1, 1, 1, 0, XCB_WINDOW_CLASS_INPUT_OUTPUT, XCB_COPY_FROM_PARENT, 0, nullptr));
    if(err.get()) {
        LOG(WARNING) << "XSETTINGS window creation error: " << xcb_event_get_error_label(err.get()->error_code);
        return;
    }
    //atoms
    xcb_atom_t xs_prop_atom = priv::p_intern_atom(conn, "_XSETTINGS_SETTINGS");
    xcb_atom_t xs_sel_atom = priv::p_intern_atom(conn, "_XSETTINGS_S0");
    if(!xs_prop_atom || !xs_sel_atom) {
        LOG(WARNING) << "XSETTINGS intern atom error";
        return;
    }
    //property
    auto propval = priv::build_data(xsettings);
    err.pop() = xcb_request_check(conn, xcb_change_property_checked(conn, XCB_PROP_MODE_REPLACE, wid, xs_prop_atom, xs_prop_atom, 8, propval.first, propval.second.get()));
    if(err.get()) {
        LOG(WARNING) << "Cannot set XSETTINGS property: " << xcb_event_get_error_label(err.get()->error_code);
        return;
    }
    //set owner
    err.pop() = xcb_request_check(conn, xcb_set_selection_owner_checked(conn, wid, xs_sel_atom, XCB_TIME_CURRENT_TIME));
    if(err.get()) {
        LOG(WARNING) << "Cannot set XSETTINGS selection owner: " << xcb_event_get_error_label(err.get()->error_code);
    }
}

pair<uint32_t,unique_ptr<uint8_t,free_delete>> priv::build_data(map<string, XSettingItem> &xsettings) {
    auto padlen = [](uint32_t len) -> uint32_t {
        return (4 - (len % 4)) % 4;
    };
    auto strtlen = [&padlen](string str) -> uint32_t {
        return str.size() + padlen(str.size());
    };
    //calculate size
    //XSETTINGS header
    uint32_t sz = 12;
    //nitems
    for(auto& kvp : xsettings) {
        //item header
        sz += 8 + strtlen(kvp.first);
        //value size depends on type
        switch (kvp.second.type) {
            case XSettingItemType::COLOR:
                sz += 8;
                break;
            case XSettingItemType::STRING:
                sz += strtlen(kvp.second.str_value) + 4;
                break;
            case XSettingItemType::INTEGER:
            default:
                //assume everything else is of "integer" type
                sz += 4;
        }
    }
    //allocate
    pair<uint32_t,unique_ptr<uint8_t,free_delete>> ret;
    ret.first = sz;
    ret.second = make_unique_malloc<uint8_t>((uint8_t*)calloc(1, sz));
    auto pret = ret.second.get();

    //host endian test
    uint32_t test = 1;
    bool is_le = ((uint8_t*)&test)[0];

    //begin write - header
    pret[0] = is_le ? 0 : 1;
    pret += 4;
    //serial (fixed to 1)
    ((uint32_t*)pret)[0] = 1;
    pret += 4;
    //nsettings
    ((uint32_t*)pret)[0] = xsettings.size();
    pret += 4;
    //records
    for(auto& kvp : xsettings) {
        //record header - type
        pret[0] = (uint8_t)kvp.second.type;
        pret += 2;
        //record header - name len
        ((uint16_t*)pret)[0] = kvp.first.size();
        pret += 2;
        //record header - name value
        //copy without trailing null
        memcpy(pret, kvp.first.c_str(), kvp.first.size());
        pret += strtlen(kvp.first);
        //record header - serial (fixed to 1)
        ((uint32_t*)pret)[0] = 1;
        pret += 4;

        //record value
        switch (kvp.second.type) {
            case XSettingItemType::INTEGER:
                ((int32_t*)pret)[0] = kvp.second.int_value;
                pret += 4;
                break;
            case XSettingItemType::COLOR:
                ((uint16_t *)pret)[0] = kvp.second.color_value.r;
                ((uint16_t *)pret)[1] = kvp.second.color_value.b;
                ((uint16_t *)pret)[2] = kvp.second.color_value.g;
                ((uint16_t *)pret)[3] = kvp.second.color_value.a;
                pret += 8;
                break;
            case XSettingItemType ::STRING:
                ((uint32_t*)pret)[0] = kvp.second.str_value.size();
                pret += 4;
                memcpy(pret, kvp.second.str_value.c_str(), kvp.second.str_value.size());
                pret += strtlen(kvp.second.str_value);
                break;
            default:
                LOG(FATAL) << "Got illegal values from XSETTINGS config parser.";
                break;
        }
    }

    assert(((size_t)(pret - ret.second.get())) == sz);

    return ret;
}

xcb_atom_t priv::p_intern_atom(xcb_connection_t* conn, string str) {
    UniquePointerWrapper_free<xcb_generic_error_t> err;
    auto rep = make_unique_malloc(xcb_intern_atom_reply(conn, xcb_intern_atom(conn, 0, str.size(), str.c_str()), &err.pop()));
    if(err.get()) {
        return XCB_ATOM_NONE;
    }
    return rep->atom;
}