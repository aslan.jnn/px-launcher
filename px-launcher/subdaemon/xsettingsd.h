#pragma once

#include <xcb/xcb.h>
#include "config.h"

namespace subdaemon {
    class XSettingsDaemon {
    public:
        XSettingsDaemon();
        ~XSettingsDaemon();
        XSettingsDaemon(const XSettingsDaemon&) = delete;
        XSettingsDaemon& operator=(const XSettingsDaemon&) = delete;
        void create_xsettings_window(xcb_connection_t* conn, xcb_screen_t* scr, std::map<std::string,XSettingItem>& xsettings);
    };
}