#include <cstdio>
#include <iostream>
#include "util.h"
#include <glog/logging.h>
#include "shared/i18n.h"
#include "shared/arraytoken.h"
#include "config.h"
#include <cstring>
#include <sys/stat.h>
#include "daemon.h"
#include "shared/util.h"
#include "ipcclient.h"
#include "shared/xcomponent.h"
#include "shared/nulldelimiterbuilder.h"
#include "shared/nulltoken.h"
#include "client.h"
#include <dlfcn.h>
#include "ipcutil.h"

using namespace std;
using namespace px;
using namespace px::util;
using namespace px::xutil;
using namespace ipcshared;

#define REVISION 10
#define DEVREV 0

void init_I18N() {
    char* tmp;
    // make sure every numeric is formatted with 'C' standard
    tmp = setlocale(LC_ALL, "C");
    // but we still want to speak local languages
    tmp = setlocale(LC_MESSAGES, "");

    // gettext magic!
    tmp = bindtextdomain("px-launcher", nullptr);
    tmp = textdomain("px-launcher");
}

void print_version() {
    puts(_("px_launcher, A launcher and manager service for px_wm."));
    fputs(_("Revision: "), stdout);
    printf("%d", REVISION);
    if(DEVREV) {
        printf("-dev-%d", DEVREV);
    }
    putchar('\n');
    printf(_("System API version: %1$d"), ipcshared::api_revision);
    puts("\n");
    puts(_("Development, unstable version. Not intended for usage outside testing machine."));
    puts(_("Make sure that both px_preloader and px_launcher are compiled from the exact same source tree, or undefined behaviour may happen."));
    puts(_("For use with px_wm revision 26 or newer and IPC version 2."));
    putchar('\n');
}

void print_usage() {
    print_version();
    puts(_("Usage:"));
    auto indputs = [](const char* msg, int spaces){
        for(int i=0; i<spaces; ++i) {
            putchar(' ');
        }
        puts(msg);
    };
    indputs(("1. px_launcher --daemon [--config <path>]"), 2);
    indputs(("2. px_launcher --reload-whitelist"), 2);
    indputs(("3. px_launcher <command> [ARGS]..."), 2);
    putchar('\n');
    indputs(_("1. Start in daemon mode, optionally specify configuration file."), 2);
    indputs(_("2. Use to ask the currently running daemon to reload the config."), 2);
    indputs(_("At the moment, only changes to whitelist are supported."), 5);
    indputs(_("Modification to group settings, number of groups, XSETTINGS,"), 5);
    indputs(_("and resolutions settings are not supported yet."), 5);
    indputs(_("3. Use to directly run the specified command in scaled mode."), 2);
}

void start_daemon(string config_path) {
    fputs(_("Using configuration file: "), stdout);
    puts(config_path.c_str());
    Daemon daemon{config_path};
    if(daemon.init_success()) {
        daemon.run();
    }
}

void ask_preloader_no_dpy_redir() {
    typedef void (*void_fn) (void);
    void_fn preloader_req_unredir;
    preloader_req_unredir = (void_fn)dlsym(RTLD_NEXT, "pxpr_request_unredirect");
    if(preloader_req_unredir) {
        preloader_req_unredir();
    }
}

int main(int argc, char ** argv) {
    init_I18N();
    //init glog
    // by default, don't log anything except 'FATAL' to stderr
    FLAGS_stderrthreshold = 3;
    google::InitGoogleLogging(argv[0]);
    //if already preloaded, request px_preload not to intercept this xcb_connect
    ask_preloader_no_dpy_redir();

    //parse arg
    px::ArrayToken argtoken(argc - 1, &argv[1]);
    if(!argtoken.is_end()) {
        string config_path = ConfigManager::default_config_file();
        string main_token = argtoken.next_token();
        if(main_token == "--daemon") {
            if(!argtoken.is_end()) {
                //get custom config path
                main_token = argtoken.next_token();
                if(main_token == "--config" && !argtoken.is_end()) {
                    config_path = argtoken.next_token();
                } else {
                    print_usage();
                    return 0;
                }
            }
            //suid before starting daemon
            int euid = geteuid();
            //warn
            if(euid != 0) {
                puts(_("Running as a regular user. Privileged program detection may fail."));
            }
            if(getuid() != euid) {
                if(setuid(euid) < 0) {
                    printf(_("setuid to %1$d failed."), euid);
                } else {
                    printf(_("setuid to %1$d success."), euid);
                }
                putchar('\n');
            }
            start_daemon(config_path);
            puts("Daemon stopped. Exiting.");
        } else if (main_token == "--reload-whitelist") {
            pxlnclient::ask_reload();
        } else if (main_token == "--version") {
            print_version();
        } else {
            //as client
            pxlnclient::exec(argc, argv);
        }
    } else {
        print_usage();
    }
    return 0;
}