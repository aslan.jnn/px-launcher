#include "util.h"
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <algorithm>

using namespace std;

string Util::get_home_directory() {
    char* home = getenv("HOME");
    if(home) {
        return set_trail_slash(home);
    } else {
        //let's consult passwd
        struct passwd* pw = getpwuid(getuid());
        return set_trail_slash(pw ? pw->pw_dir : "/");
    }
}

string Util::set_trail_slash(string path) {
    return (*(path.end() - 1) == '/' ? path : path + '/');
}

void StringUtil::ltrim(string &s) {
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
}

void StringUtil::rtrim(string &s) {
    s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
}

void StringUtil::trim(string &s) {
    ltrim(s);
    rtrim(s);
}