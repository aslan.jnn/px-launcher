#pragma once

#include <cstdint>
#include <functional>
#include <memory>
#include "config.h"
#include "shared/xcomponent.h"

namespace subdaemon {
    class SizeScaleMonitor {
    public:
        SizeScaleMonitor(px::xutil::XDisplayComponent fedpy);
        ~SizeScaleMonitor();
        SizeScaleMonitor(const SizeScaleMonitor&) = delete;
        SizeScaleMonitor& operator=(const SizeScaleMonitor&) = delete;
        bool is_usable();
        uint16_t current_scfactor();
        std::pair<uint16_t,uint16_t> current_size_1x();
        //callbacks will only be called if and only if their respective value changes
        void run(std::function<void(uint16_t)> rescale_callback, std::function<void(std::pair<uint16_t,uint16_t>)> resize_callback);
        void stop();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}