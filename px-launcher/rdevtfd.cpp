#include "rdevtfd.h"
#include <sys/eventfd.h>
#include <unistd.h>

ReadEventFD::ReadEventFD() : fd{eventfd(0, 0)} {
    if(fd < 0) {
        //error
        return;
    }
}

ReadEventFD::~ReadEventFD() {
    interrupt();
    close(fd);
}

void ReadEventFD::clear() {
    fd_set fs;
    struct timespec notimeout {0, 0};
    int sel;
    //poll for existing data. guaranteed not to error, except on some OOM situations / racing with a SIGNAL.
    do {
        FD_ZERO(&fs);
        FD_SET(fd, &fs);
        sel = pselect(fd + 1, &fs, nullptr, nullptr, &notimeout, nullptr);
    } while (sel < 0);
    if(sel) {
        //since this is not a EFD_SEMAPHORE, a single read is enough to clear this eventfd,
        uint64_t rd;
        read(fd, &rd, 8);
    }
}

void ReadEventFD::interrupt() {
    uint64_t wr = 1;
    write(fd, &wr, 8);
}

void ReadEventFD::wait() {
    wait(nullptr);
}

void ReadEventFD::wait(struct timespec* timeout) {
    fd_set fs;
    FD_ZERO(&fs);
    FD_SET(fd, &fs);
    pselect(fd + 1, &fs, nullptr, nullptr, timeout, nullptr);
}