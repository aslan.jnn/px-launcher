#pragma once

#include <string>

class Log {
public:
    int pid;
    int ruid;
    int euid;
    std::string cmdline;

    Log();
    Log(const Log&) = delete;
    Log& operator=(const Log&) = delete;

    void append_log(const char* target_dpy, const std::string msg);
};