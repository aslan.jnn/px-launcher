#pragma once

#include <cstdint>
#include <utility>
#include <vector>
#include <string>
#include <memory>
#include "config.h"
#include "shared/xcomponent.h"

namespace subdaemon {
    class DisplayManager {
    public:
        //running out of time: instead of directly parse the config constants, ask the caller to provide the param instead!
        DisplayManager(px::xutil::XDisplayComponent fe_dpy, uint16_t scfactor, std::pair<uint16_t,uint16_t> size_1x, std::string scale_alg, std::string fr_mode, std::map<std::string,XSettingItem>& xsettings);
        DisplayManager(const DisplayManager&) = delete;
        DisplayManager& operator=(const DisplayManager&) = delete;
        ~DisplayManager();
        /**
         * Call if frontend's scale factor changed.
         */
        void rescale(uint16_t scfactor);
        /**
         * Call if frontend's root window size, after divided by scaling factor, changed.
         */
        void resize(std::pair<uint16_t,uint16_t> size_1x);
        /**
         * Get the display string for the applications to connect to.
         * @return The display string of the created backend display.
         */
        std::string get_display_group_name();

    private:
        class Priv;
        std::unique_ptr<Priv> p;
    };
}