#pragma once

#include <functional>

namespace sighandlers {
    void install_termination_signal_handlers(std::function<void(void)> callback);
}