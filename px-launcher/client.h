#pragma once

#include "shared/xcomponent.h"

namespace pxlnclient {
    px::xutil::XDisplayComponent current_frontend_dpy();
    void ask_reload();
    void exec(int argc, char ** argv);
}