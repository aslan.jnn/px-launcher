#include "ipcserver.h"
#include "ipcutil.h"
#include <sys/socket.h>
#include <sys/un.h>
#include <cstdio>
#include <glog/logging.h>
#include "shared/i18n.h"
#include "shared/util.h"
#include <atomic>
#include "rdevtfd.h"
#include <algorithm>
#include <errno.h>
#include <sys/stat.h>

using namespace std;
using namespace ipcshared;
using namespace px::util;

class IPCServer::Priv {
public:
    string serveraddr;
    int server_fd;
    bool ready = false;
    px::xutil::XDisplayComponent display{""};

    //thread control variables
    ReadEventFD reqstop_waiter;
    ReadEventFD destructor_waiter;
    atomic<bool> running;
    atomic<bool> req_stop;

    bool create_and_bind_socket();
    void send_reply(uint16_t reply_id, msg::MessageStore rep);
};

IPCServer::IPCServer(px::xutil::XDisplayComponent& display) {
    p = make_unique<Priv>();
    p->display = display;
    //thread control variables
    p->running = false;
    p->req_stop = false;
    //listen on construct
    p->ready = p->create_and_bind_socket();
}

IPCServer::~IPCServer() {
    if(p->running.load(memory_order_seq_cst)) {
        //request stop and wait until fully stopped
        stop();
        p->destructor_waiter.wait();
    }
    //unbind, unlink socket
    close(p->server_fd);
    unlink(p->serveraddr.c_str());
}

bool IPCServer::is_ready() {
    return p->ready;
}

void IPCServer::run(function<msg::MessageStore(msg::MessageStore)> callback) {
    if(p->running.load(memory_order_seq_cst)) {
        return;
    }
    p->req_stop = false;
    p->running = true;
    p->reqstop_waiter.clear();
    p->destructor_waiter.clear();

    //request (we receive)
    bool req_payload = false;
    msg::MessageStore req;
    msgreq::RequestHeader* reqhdr = (msgreq::RequestHeader*)req.header.data;
    //reply (we send)
    msg::MessageStore rep;

    //PLAN: this server can be tricked to wait for payload indefinitely
    fd_set rfds;
    int select_maxfd = max({p->server_fd, p->reqstop_waiter.fd}) + 1;
    while (true) {
        FD_ZERO(&rfds);
        FD_SET(p->server_fd, &rfds);
        FD_SET(p->reqstop_waiter.fd, &rfds);
        //wait indefinitely
        if(pselect(select_maxfd, &rfds, nullptr, nullptr, nullptr, nullptr) < 0) {
            if(errno == EINTR) {
                //continue waiting on signal
                continue;
            }
        }
        //check if we're asked to stop
        if(FD_ISSET(p->reqstop_waiter.fd, &rfds) && p->req_stop.load(memory_order_seq_cst)) {
            break;
        }
        //data arriving?
        if(FD_ISSET(p->server_fd, &rfds)) {
            void* buff_rd;
            size_t to_rd;
            if(req_payload) {
                to_rd = reqhdr->payload_sz;
                req.payload.insert(req.payload.begin(), to_rd, 0);
                buff_rd = &req.payload[0];
            } else {
                req.payload.clear();
                to_rd = msg::MSGHDR_MAX_SIZE;
                buff_rd = req.header.data;
            }
            read(p->server_fd, buff_rd, to_rd);

            //do something with the received data
            if(!req_payload) {
                if(reqhdr->payload_sz) {
                    req_payload = true;
                } else {
                    p->send_reply(reqhdr->reply_to, callback(req));
                }
            } else {
                req_payload = false;
                p->send_reply(reqhdr->reply_to, callback(req));
            }
        }
    }
    p->running = false;
    p->destructor_waiter.interrupt();
}

void IPCServer::stop() {
    if(!p->running.load(memory_order_seq_cst) || p->req_stop.load(memory_order_seq_cst)) {
        return;
    }
    p->req_stop = true;
    p->reqstop_waiter.interrupt();
}

bool IPCServer::Priv::create_and_bind_socket() {
    server_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(server_fd < 0) {
        perror(_("Cannot open socket"));
        LOG(ERROR) << "Cannot open socket: " << strerror(errno);
        return false;
    }

    //try to create the appropriate directory
    bool mkdir_success = false;
    if(mkdir(ipc_basedir.c_str(), 0777) == 0){
        mkdir_success = true;
    }
    //check the IPC directory and chmod if necessary
    auto warn_ipc_dir = []() {
        puts(_("Program may not work correctly."));
        printf(_("Please check the '%1$s' directory and it's permission."), ipc_basedir.c_str());
        putchar('\n');
    };
    struct stat ipcdir_stat;
    if(stat(ipc_basedir.c_str(), &ipcdir_stat) < 0) {
        warn_ipc_dir();
    } else {
        if((ipcdir_stat.st_mode & 0777) != 0777) {
            if(chmod(ipc_basedir.c_str(), 0777) < 0) {
                warn_ipc_dir();
            }
        }
    }

    sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    serveraddr = ipcshared::get_ipc_server_addr(display);
    strcpy(addr.sun_path, serveraddr.c_str());

    //bind
    for (int i = 0; i <= 1; ++i) {
        int bind_stat = bind(server_fd, (sockaddr*)&addr, sizeof(sockaddr_un));
        if (bind_stat < 0) {
            if (i) {
                // unrecoverable failure. cannot bind at all.
                perror(_("Cannot bind socket"));
                LOG(ERROR) << "Cannot bind socket: " << strerror(errno);
                return false;
            } else {
                // retry by unlink the file first
                if (unlink(serveraddr.c_str()) < 0) {
                    perror(_("Cannot bind socket"));
                    LOG(ERROR) << "Cannot bind socket after unlink: " << strerror(errno);
                    return false;
                }
            }
        } else {
            // bind success!
            if(i) {
                LOG(INFO) << "Server socket bound successfully after unlink.";
            }
            //chmod all access, so that LD_PRELOAD-ed apps can communicate
            chmod(serveraddr.c_str(), 0777);
            break;
        }
    }

    return true;
}

void IPCServer::Priv::send_reply(uint16_t reply_id, msg::MessageStore rep) {
    sockaddr_un rep_addr;
    rep_addr.sun_family = AF_UNIX;
    strcpy(rep_addr.sun_path, get_ipc_reply_addr(reply_id).c_str());

    //just to make sure
    auto rephdr = (msgrep::ReplyHeader*)rep.header.data;
    rephdr->payload_sz = rep.payload.size();

    //reply ch
    int reply_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(reply_fd < 0) {
        LOG(ERROR) << "Cannot open socket for replying.";
    }
    DeleteInvoker reply_fd_deletor([&reply_fd](){
        close(reply_fd);
    });

    //send
    //header
    if(sendto(reply_fd, rephdr, msg::MSGHDR_MAX_SIZE, 0, (sockaddr*)&rep_addr, sizeof(sockaddr_un)) < 0) {
        LOG(ERROR) << "Failed to send reply header.";
    }
    //payload
    if(rephdr->payload_sz) {
        if(sendto(reply_fd, &rep.payload[0], rephdr->payload_sz, 0, (sockaddr*)&rep_addr, sizeof(sockaddr_un)) < 0) {
            LOG(ERROR) << "Failed to send reply payload.";
        }
    }
}