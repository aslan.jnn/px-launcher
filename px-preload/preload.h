#pragma once

#include <xcb/xcb.h>

extern "C" {
    /*internal functions*/
    void pxpr_request_unredirect();
    uint32_t pxpr_system_api_version();

    /*functions to be preloaded*/
    xcb_connection_t *xcb_connect(const char *displayname, int *screenp);
}