#pragma once

#include <string>
#include <algorithm>
#include <cstddef>
#include <vector>

//do not include terminating null character in IPC size, if the expected data is string.
namespace ipcshared {
    namespace msgreq {
        enum class RequestType : uint8_t {NONE, PING, QUERY_BK, ASK_DISPLAY, RELOAD_CFG};

        struct RequestHeader {
            RequestType type = RequestType::NONE;
            uint16_t reply_to;
            uint32_t payload_sz = 0;

            RequestHeader(RequestType p_type) : type{p_type} {}
        };

        enum class DisplayRequestorType : uint8_t {ONDEMAND, PRELOADER};
        struct AskDisplayRequest {
            RequestHeader header {RequestType::ASK_DISPLAY};
            pid_t pid;
            DisplayRequestorType requestor_type;
        };

        constexpr int REQUEST_MAX_SIZE = std::max({sizeof(AskDisplayRequest)});
    }

    namespace msgrep {
        enum class ReplyType : uint8_t {NONE, PING_REPLY, BK_LIST, DISPLAY_REPLY, RELOAD_CFG};

        struct ReplyHeader {
            ReplyType type = ReplyType::NONE;
            uint32_t payload_sz = 0;

            ReplyHeader(ReplyType p_type) : type{p_type} {}
        };

        struct AskDisplayReply {
            ReplyHeader header {ReplyType::DISPLAY_REPLY};
            //Indicates whether client should redirect the display param. Treat as bool.
            uint8_t change;
        };

        struct ReloadCfgReply {
            ReplyType type = ReplyType::RELOAD_CFG;
            //Indicates that daemon has reloaded the config file successfully. Treat as bool.
            uint8_t success;
        };

        constexpr int REPLY_MAX_SIZE = std::max({sizeof(AskDisplayReply), sizeof(ReloadCfgReply)});
    }

    namespace msg {
        //to store both Request and Reply header
        constexpr int MSGHDR_MAX_SIZE = std::max(msgreq::REQUEST_MAX_SIZE, msgrep::REPLY_MAX_SIZE);
        typedef struct alignas(alignof(std::max_align_t)) { uint8_t data[MSGHDR_MAX_SIZE]; } command_store_t;

        struct MessageStore {
            command_store_t header;
            std::vector<uint8_t> payload;
        };
    }
}