#include "config.h"
#include "util.h"
#include <cstdio>
#include "shared/i18n.h"
#include <glog/logging.h>
#include <vector>
#include <map>
#include "shared/util.h"

using namespace std;
using namespace px::util;

class SplitString {
public:
    SplitString(string line);
    SplitString(string line, char delim);
    const vector<string> result;
private:
    vector<string> parse(string line, char delim);
};

namespace priv {
    void print_parse_error(string err_cause) {
        fputs(_("Error parsing configuration"), stderr);
        fputs(": ", stderr);
        fputs(err_cause.c_str(), stderr);
        fputc('\n', stderr);
    }
}

namespace catparser {
    //all methods here take the same signature: SplitParam&, int&, Config&
    //1st is input params, 2nd is context (if set to zero, they'll assume this is a category header), 3rd is the config object to populate.
    typedef bool (*CatParserFn)(SplitString&,int&,Config&);

    bool group(SplitString &param, int &context, Config &ret) {
        //cat header
        if(!context) {
            //string to value map
            static map<string,ScalingAlgorithm> scalealg_map {{"nearest", ScalingAlgorithm::NEAREST}, {"bilinear", ScalingAlgorithm::BILINEAR}, {"raa", ScalingAlgorithm::RAA}};
            static map<string,FrameRateMode> framerate_map {{"low", FrameRateMode::LOW}, {"normal", FrameRateMode::NORMAL}, {"high", FrameRateMode::HIGH}};

            //for header, we parse this group's setting
            MatchGroupSetting setting;
            //scalealg
            if(param.result.size() >= 2) {
                map<string,ScalingAlgorithm>::iterator sa_it;
                if(check_and_get_mapped_value(scalealg_map, param.result[1], sa_it)) {
                    setting.scalealg = sa_it->second;
                } else {
                    priv::print_parse_error(_("Invalid or unsupported scaling algorithm"));
                    LOG(ERROR) << "Parse error: Invalid scaling algorithm value";
                    return false;
                }
            }
            //framerate
            if(param.result.size() >= 3) {
                map<string,FrameRateMode>::iterator fr_it;
                if(check_and_get_mapped_value(framerate_map, param.result[2], fr_it)) {
                    setting.framerate = fr_it->second;
                } else {
                    priv::print_parse_error(_("Invalid frame rate mode specified"));
                    LOG(ERROR) << "Parse error: Invalid frame rate mode value";
                    return false;
                }
            }

            //create a new group
            ret.groups.push_back(MatchGroup{.setting = setting});
            context = ret.groups.size();
            return true;
        }

        //cat body
        //'context' is assumed to be always correct :)
        uint16_t gidx = context - 1;
        if(param.result.size() != 2) {
            priv::print_parse_error(_("Invalid param count"));
            LOG(ERROR) << "Parse error: invalid param count";
            return false;
        }
        long int parental = strtol(param.result[0].c_str(), nullptr, 10);
        //we limit to max 8 parents
        if(parental > 8) {
            priv::print_parse_error(_("Parental value out of range"));
            LOG(ERROR) << "Parse error: Parental value out of range";
            return false;
        } else if (parental < 0) {
            parental = 0;
        }
        if(parental > ret.max_parent_level) {
            for(int i = ret.max_parent_level; i < parental; ++i) {
                ret.abspath_match.push_back({});
                ret.execname_match.push_back({});
            }
            ret.max_parent_level = parental;
        }

        if(*param.result[1].begin() == '/') {
            static char realpath_ret[4096];
            realpath(param.result[1].c_str(), realpath_ret);
            ret.abspath_match[parental][realpath_ret] = gidx;
        } else {
            ret.execname_match[parental][param.result[1]] = gidx;
        }
        return true;
    }

    bool xsettings(SplitString &param, int &context, Config &ret) {
        //we have nothing at all in header
        if(!context) {
            context = 1;
            return true;
        }
        //basic param check
        if(param.result.size() < 3) {
            priv::print_parse_error(_("Invalid XSETTINGS item detail count"));
            LOG(ERROR) << "Parse error: Invalid XSETTINGS item detail count";
            return false;
        }
        //0 - title
        XSettingItem item;
        //item.name = param.result[0];
        //1 - type
        if(param.result[1] == "int") {
            item.type = XSettingItemType::INTEGER;
            try {
                item.int_value = stoi(param.result[2]);
            } catch (const exception& e) {
                priv::print_parse_error(_("Invalid int value in XSETTINGS"));
                LOG(ERROR) << "Parse error: Invalid int value in XSETTINGS";
                return false;
            }
        } else if(param.result[1] == "string") {
            stringstream ss;
            //to handle values with colon
            for(int i = 2; i < param.result.size() - 1; ++i) {
                ss << param.result[i] << ":";
            }
            ss << param.result[param.result.size() - 1];
            item.type = XSettingItemType::STRING;
            item.str_value = ss.str();
        } else if(param.result[1] == "color") {
            //skip
            if(context < 2) {
                ++context;
                puts(_("XSETTINGS item with 'color' type isn't supported and won't be loaded."));
            }
            return true;
        } else {
            priv::print_parse_error(_("Invalid XSETTINGS item type"));
            LOG(ERROR) << "Parse error: Invalid XSETTINGS item type";
            return false;
        }
        //parse OK
        ret.xsettings[param.result[0]] = item;
        return true;
    }

}

string ConfigManager::default_config_file() {
    return Util::get_home_directory() + ".config/px-launcher.conf";
}

Config ConfigManager::from_file(std::string path) {
    //category string ID to category parser function mapping
    typedef map<string,catparser::CatParserFn> cat_parser_map_t;
    static cat_parser_map_t cat_parser_map {{"*whitelist", catparser::group}, {"*xsettings", catparser::xsettings}};

    Config ret;
    auto fh = fopen(path.c_str(), "r");
    if(!fh) {
        perror(_("Cannot open configuration file"));
        LOG(ERROR) << "Cannot open configuration file: " << strerror(errno);
        return ret;
    }

    //function pointer to category parsers
    catparser::CatParserFn cat_parser = nullptr;
    int cat_parser_context = 0;

    //begin read line by line
    char* line_raw = nullptr;
    size_t line_count = 0;
    ssize_t line_read;
    while ((line_read = getline(&line_raw, &line_count, fh)) >= 0) {
        if(!line_read) {
            continue;
        }
        //trim
        string line = line_raw;
        StringUtil::trim(line);
        if(!line.size()) {
            continue;
        }

        //check if comment
        if(*line.begin() == '#') {
            continue;
        }

        //tokenize!
        SplitString params(line);
        //check for setting category
        if(*params.result[0].begin() == '*') {
            cat_parser_map_t::iterator it;
            if(check_and_get_mapped_value(cat_parser_map, params.result[0], it)) {
                cat_parser = it->second;
                cat_parser_context = 0;
            } else {
                priv::print_parse_error(_("Unknown settings category"));
                LOG(ERROR) << "Parse error: Unknown settings category";
                return ret;
            }
        }
        //ask each respective category parser
        if(cat_parser) {
            //make sure the cat_parser is OK with the data before proceeding
            //let each cat_parser log it's own error
            if(!cat_parser(params, cat_parser_context, ret)) {
                return ret;
            }
        }
    }

    //we've made this far, so this Config should be OK and ready to use.
    ret.is_complete = true;
    return ret;
}

SplitString::SplitString(string line) : result{parse(line, ':')} {}

SplitString::SplitString(string line, char delim) : result{parse(line, delim)} {}

vector<string> SplitString::parse(string line, char delim) {
    stringstream ss(line);
    string item;
    vector<string> ret;
    while(getline(ss, item, delim)) {
        ret.push_back(item);
    }
    return ret;
}