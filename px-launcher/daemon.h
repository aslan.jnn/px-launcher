#pragma once

#include <memory>
#include <string>

class Daemon {
public:
    Daemon(std::string config_file);
    Daemon(const Daemon&) = delete;
    Daemon& operator=(const Daemon&) = delete;
    ~Daemon();
    bool init_success();
    void run();
    void stop();

private:
    class Priv;
    class NTHandler;
    std::unique_ptr<Priv> p;
    std::unique_ptr<NTHandler> nth;
};