#include "sighandler.h"
#include <signal.h>
#include <atomic>
#include <initializer_list>
#include <cstdio>
#include <cstdlib>

using namespace std;

namespace sighandlers {
    atomic<bool> terminating {false};
    function<void(void)> callback;

    void termination_signal_handler(int sig);
}

void sighandlers::install_termination_signal_handlers(function<void(void)> p_callback) {
    struct sigaction sa;
    for (int i : {SIGINT, SIGTERM, SIGQUIT, SIGHUP}) {
        sa.sa_handler = termination_signal_handler;
        sigaction(i, &sa, nullptr);
    }
    callback = p_callback;
}

void sighandlers::termination_signal_handler(int sig) {
    if(terminating.load(memory_order_seq_cst)) {
        return;
    }
    terminating.store(true, memory_order_seq_cst);
    callback();
    //everything should exit by itself on deconstruct
}