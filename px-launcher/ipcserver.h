#pragma once

#include <functional>
#include <memory>
#include "shared/xcomponent.h"
#include "ipcmsg.h"

class IPCServer {
public:
    IPCServer(px::xutil::XDisplayComponent& display);
    IPCServer(const IPCServer&) = delete;
    IPCServer& operator=(const IPCServer&) = delete;
    ~IPCServer();
    bool is_ready();
    void run(std::function<ipcshared::msg::MessageStore(ipcshared::msg::MessageStore)> callback);
    void stop();

private:
    class Priv;
    std::unique_ptr<Priv> p;
};