#include "dpymgr.h"
#include <unordered_map>
#include <unistd.h>
#include <xcb/xcb.h>
#include <mutex>
#include <atomic>
#include <glog/logging.h>
#include "shared/util.h"
#include "rdevtfd.h"
#include "shared/memory.h"
#include "shared/util.h"
#include <sys/wait.h>
#include <thread>
#include "ipcutil.h"
#include "raii/process.h"
#include <memory>
#include "shared/i18n.h"
#include "xsettingsd.h"
#include "randr.h"

using namespace subdaemon;
using namespace std;
using namespace px::util;
using namespace px::xutil;
using namespace px::memory;

class DisplayManager::Priv{
public:
    class ConnMgr {
    private:
        xcb_connection_t* conn_data;
        xcb_screen_t* scr_data;
        mutex data_mutex;
        void free_data();
    public:
        ConnMgr() : conn_data{nullptr}{}
        ConnMgr(const ConnMgr&) = delete;
        ConnMgr& operator=(const ConnMgr&) = delete;
        ~ConnMgr();
        xcb_connection_t* get_conn();
        xcb_screen_t* get_scr();
        void set(xcb_connection_t* conn, xcb_screen_t* scr);
    };

    struct Size {
        uint16_t width;
        uint16_t height;
    };

    atomic<uint16_t> current_scfactor;
    pair<uint16_t, uint16_t> current_size_1x;
    atomic<Size> current_size_1x_sh;
    atomic<bool> reqstop {false};
    thread runner_thread;
    string scale_alg;
    string fr_mode;
    map<string,XSettingItem> xsettings;
    RandRSetting xrandr_setter;

    //current active group (only one)
    const string ag_dpystr = generate_dpystr();
    //for px_wm
    string pxwm_instance_path;
    //only runner_thread is supposed to setup this value
    ConnMgr ag_conn;

    void rescale_pxwm(uint16_t scfactor);
    static string generate_dpystr();
    static string generate_pxwm_instance_path(XDisplayComponent dpy);
    void runner_thread_fn();
};

DisplayManager::DisplayManager(XDisplayComponent fe_dpy, uint16_t scfactor, pair<uint16_t, uint16_t> size_1x, string scale_alg, string fr_mode, map<string,XSettingItem>& xsettings) {
    p = make_unique<Priv>();
    p->current_scfactor = scfactor;
    p->current_size_1x = size_1x;
    p->current_size_1x_sh = Priv::Size{size_1x.first, size_1x.second};
    p->scale_alg = scale_alg;
    p->fr_mode = fr_mode;
    p->xsettings = xsettings;
    p->pxwm_instance_path = Priv::generate_pxwm_instance_path(fe_dpy);
    //this thread must be @ bottommost of this constructor, otherwise instance data may be incomplete because of race conditions.
    p->runner_thread = thread(bind(&Priv::runner_thread_fn, p.get()));
}

DisplayManager::~DisplayManager() {
    //stop runner thread and wait (stop px-wm using IPC remove all bk)
    p->reqstop = true;
    vector<string> pxwm_stop_argv {"px_wm", "--socketfile", p->pxwm_instance_path, "--manage", "remove-all-backend"};
    raii::Process(pxwm_stop_argv, {}).wait();
    if(p->runner_thread.joinable()) {
        p->runner_thread.join();
    }
}

void DisplayManager::resize(pair<uint16_t, uint16_t> size_1x) {
    if(p->current_size_1x != size_1x) {
        p->current_size_1x = size_1x;
        p->current_size_1x_sh = Priv::Size{size_1x.first, size_1x.second};
        p->xrandr_setter.resize_display(size_1x);
    }
}

void DisplayManager::rescale(uint16_t scfactor) {
    if(p->current_scfactor.load(memory_order_relaxed) != scfactor) {
        p->current_scfactor = scfactor;
        p->rescale_pxwm(scfactor);
    }
}

std::string DisplayManager::get_display_group_name() {
    return p->ag_dpystr;
}

void DisplayManager::Priv::rescale_pxwm(uint16_t scfactor) {
    vector<string> pxwm_cmd_argv {"px_wm", "--socketfile", pxwm_instance_path, "--manage", "rescale-backend", ag_dpystr, to_string(scfactor)};
    raii::Process(pxwm_cmd_argv, {}).wait();
}

string DisplayManager::Priv::generate_dpystr() {
    uint32_t dpynum = 90;
    while (true) {
        string dpyaddr = px::util::string_printf(":%d", dpynum++);
        auto test_conn = xcb_connect(dpyaddr.c_str(), nullptr);
        bool ok = false;
        if(xcb_connection_has_error(test_conn)) {
            ok = true;
        }
        xcb_disconnect(test_conn);
        if(ok) {
            return dpyaddr;
        }
    }
}

string DisplayManager::Priv::generate_pxwm_instance_path(XDisplayComponent dpy) {
    string ret = ipcshared::ipc_basedir + "px_wm-session-" + dpy.get_host() + ".X." + to_string(dpy.get_display()) + "." + to_string(dpy.get_screen()) + ".socket";
    return ret;
}

void DisplayManager::Priv::runner_thread_fn() {
    //child processes
    unique_ptr<raii::Process> xvfb_proc;
    XSettingsDaemon xsettingsd;
//    unique_ptr<raii::Process> xsettingsd_proc;

    auto start_xvfb = [&]() {
        vector<string> argv {"Xvfb", "-screen", "0", "8192x8192x24", ag_dpystr};
        printf(_("Started Xvfb at '%1$s'"), ag_dpystr.c_str());
        putchar('\n');
        xvfb_proc = make_unique<raii::Process>(argv, vector<pair<string, string>>{});
        xvfb_proc->signal_on_exit = SIGTERM;
        //retry connecting to Xvfb until specified timeout
        constexpr int delay_ms = 50;
        constexpr int delay_retry_max_ms = 3000;
        for(int i=0; i < (delay_retry_max_ms / delay_ms); ++i) {
            usleep(delay_ms * 1000);
            int scrno;
            xcb_connection_t* test_conn = xcb_connect(ag_dpystr.c_str(), &scrno);
            if(xcb_connection_has_error(test_conn)) {
                //fail. retry.
                xcb_disconnect(test_conn);
                continue;
            }
            //success, get the screen object
            auto svr_setup = xcb_get_setup(test_conn);
            xcb_screen_iterator_t screen_iter = xcb_setup_roots_iterator(svr_setup);
            for (int i = 0; i < scrno; ++i) {
                xcb_screen_next(&screen_iter);
            }
            //save the connection and screen objects to thread-safe storage
            ag_conn.set(test_conn, screen_iter.data);
            //initialize xsettings on the backend display
            xsettingsd.create_xsettings_window(ag_conn.get_conn(), ag_conn.get_scr(), xsettings);
            //initialize RandR setter
            xrandr_setter.update(ag_conn.get_conn(), ag_conn.get_scr());
            break;
        }
    };
    //px-wm argv
    fputs("px_wm instance path: ", stdout);
    puts(pxwm_instance_path.c_str());
    //TODO: px_wm exclusive
    vector<string> pxwm_argv {"px_wm", "--socketfile", pxwm_instance_path, "--bkdisplay", ag_dpystr, "--bkd-default-scale-factor", "1", "--bkd-scale-alg", scale_alg, "--bkd-framerate-mode", fr_mode};
    //please manually update this if the argv structure above is changed
    constexpr int pxwm_argv_scfactor_idx = 6;
    while(1) {
        //if px_wm stopped (should be because Xvfb is killed), then this loop is repeated over.
        //test if Xvfb is running and accepting clients
        bool restart_xvfb = false;
        xcb_connection_t* test_conn = xcb_connect(ag_dpystr.c_str(), nullptr);
        if(xcb_connection_has_error(test_conn)) {
            restart_xvfb = true;
        }
        xcb_disconnect(test_conn);
        //start Xvfb if it is not running (if it is, it is assumed that ag_conn is still a valid value)
        if(restart_xvfb) {
            start_xvfb();
        }
        //resize Xvfb
        auto curr_cfged_size = current_size_1x_sh.load(memory_order_seq_cst);
        xrandr_setter.resize_display({curr_cfged_size.width, curr_cfged_size.height});
        //start px-wm
        pxwm_argv[pxwm_argv_scfactor_idx] = to_string(current_scfactor.load(memory_order_seq_cst));
        raii::Process(pxwm_argv, {}).wait();
        //check if we're asked to stop
        if(reqstop.load(memory_order_seq_cst)) {
            break;
        }
        //"free" Xvfb first
        xvfb_proc = nullptr;
        LOG(WARNING) << "px_wm restarts.";
        //throttle
        sleep(1);
    }
}

DisplayManager::Priv::ConnMgr::~ConnMgr() {
    unique_lock<mutex>(data_mutex);
    free_data();
}

xcb_connection_t* DisplayManager::Priv::ConnMgr::get_conn() {
    unique_lock<mutex>(data_mutex);
    return conn_data;
}

xcb_screen_t* DisplayManager::Priv::ConnMgr::get_scr() {
    unique_lock<mutex>(data_mutex);
    return scr_data;
}

void DisplayManager::Priv::ConnMgr::set(xcb_connection_t* conn, xcb_screen_t* scr) {
    unique_lock<mutex>(data_mutex);
    free_data();
    conn_data = conn;
    scr_data = scr;
}

void DisplayManager::Priv::ConnMgr::free_data() {
    if(conn_data) {
        xcb_disconnect(conn_data);
        conn_data = nullptr;
        //no need to free screen
    }
}