#include "daemon.h"
#include "shared/xcomponent.h"
#include "shared/util.h"
#include <cstdlib>
#include <limits.h>
#include <glog/logging.h>
#include "shared/i18n.h"
#include "ipcclient.h"
#include "ipcserver.h"
#include "subdaemon/dpymgr.h"
#include "subdaemon/sizescalemon.h"
#include <thread>
#include <atomic>
#include "rdevtfd.h"
#include "config.h"
#include "shared/nulldelimiterbuilder.h"
#include "sighandler.h"

using namespace std;
using namespace px::util;
using namespace px::xutil;
using namespace ipcshared;

class Daemon::Priv {
public:
    NTHandler* nth;

    //thread control
    atomic<bool> running {false};
    atomic<bool> stopping {false};
    ReadEventFD destructor_waiter;

    XDisplayComponent display {""};
    Config cfg;
    string cfg_file_src;
    bool init_success = false;
    unique_ptr<IPCServer> server;
    unique_ptr<subdaemon::DisplayManager> dpymgr;
    unique_ptr<subdaemon::SizeScaleMonitor> szscmon;

    msg::MessageStore server_callback(msg::MessageStore request);
    void mon_sc_callback(uint16_t new_scfactor);
    void mon_sz_callback(pair<uint16_t,uint16_t> new_size_1x);
};

class Daemon::NTHandler {
    //stands for Non Trivial Handler :)
public:
    Priv* p;
    NTHandler(Priv* p) : p{p} {}

    void srvhdlr_ask_display(msg::MessageStore& req, msg::MessageStore& rep);
    void srvhdlr_reload_cfg(msg::MessageStore& rep);
};

Daemon::Daemon(string config_file) {
    p = make_unique<Priv>();
    p->cfg_file_src = config_file;
    nth = make_unique<NTHandler>(p.get());
    p->nth = nth.get();

    sighandlers::install_termination_signal_handlers(bind(&Daemon::stop, this));

    //what we want to manage?
    char* display_env = getenv("DISPLAY");
    if(!display_env) {
        fputsn(_("DISPLAY environment variable is not specified. Make sure to run this program on an existing X session."), stderr);
        LOG(ERROR) << "DISPLAY is not specified.";
        return;
    }
    p->display = XDisplayComponent(display_env);
    if(p->display.is_invalid()) {
        fputsn(_("DISPLAY environment variable is invalid."), stderr);
        LOG(ERROR) << "DISPLAY is invalid.";
        return;
    }

    //Check for existing service for this display before starting
    IPCClient checker_client(p->display);
    msg::MessageStore pingreq;
    new (pingreq.header.data) msgreq::RequestHeader(msgreq::RequestType::PING);
    auto pingrep = checker_client.request_for_reply(pingreq);
    //assume OK (doesn't exists) if error
    if(pingrep.first == IPCClient::ReplyStatus::SUCCESS) {
        fprintf(stderr, _("Service for '%1$s' is already running."), p->display.to_string().c_str());
        fputc('\n', stderr);
        LOG(ERROR) << string_printf("Service for '%s' is already running.", p->display.to_string().c_str());
        return;
    }

    //load initial config
    p->cfg = ConfigManager::from_file(config_file);
    if(!p->cfg.is_complete) {
        fputsn(_("Cannot load configuration file."), stderr);
        LOG(ERROR) << "Initial configuration loading failed.";
        return;
    }
    //at the moment, we only supports 1 group
    if(p->cfg.groups.size() != 1) {
        fputsn(_("Only 1 group are supported as of now."), stderr);
        return;
    }

    //server
    p->server = make_unique<IPCServer>(p->display);
    if(!p->server->is_ready()) {
        fputsn(_("IPC server failed to start."), stderr);
        LOG(ERROR) << "IPC server failed to start.";
        return;
    }

    //subdaemons
    p->szscmon = make_unique<subdaemon::SizeScaleMonitor>(p->display);
    if(!p->szscmon->is_usable()) {
        p->szscmon = make_unique<subdaemon::SizeScaleMonitor>(p->display);
        fputsn(_("Cannot connect to frontend display server."), stderr);
        LOG(ERROR) << "Size and scaling monitor could not connect to frontend.";
        return;
    }
    map<ScalingAlgorithm,string> map_scalg {{ScalingAlgorithm::NEAREST, "nearest"}, {ScalingAlgorithm::BILINEAR, "bilinear"}, {ScalingAlgorithm::RAA, "raa"}};
    map<FrameRateMode,string> map_frmode {{FrameRateMode::LOW, "low"}, {FrameRateMode::NORMAL, "normal"}, {FrameRateMode::HIGH, "high"}};
    p->dpymgr = make_unique<subdaemon::DisplayManager>(p->display, p->szscmon->current_scfactor(), p->szscmon->current_size_1x(), map_scalg[p->cfg.groups[0].setting.scalealg], map_frmode[p->cfg.groups[0].setting.framerate], p->cfg.xsettings);
    //OK
    p->init_success = true;
}

Daemon::~Daemon() {
    //ask to stop and wait until running thread stopped (if it is running)
    if(p->running.load(memory_order_seq_cst)) {
        stop();
        p->destructor_waiter.wait();
    }
}

bool Daemon::init_success() {
    return p->init_success;
}

void Daemon::run() {
    if(p->running.load(memory_order_seq_cst)) {
        return;
    }
    p->running = true;
    p->destructor_waiter.clear();
    //start IPC server
    auto bound_svr_cb = bind(&Priv::server_callback, p.get(), placeholders::_1);
    thread ipcsvr_thrd(&IPCServer::run, p->server.get(), bound_svr_cb);
    //start RANDR monitor
    auto bound_sz_cb = bind(&Priv::mon_sz_callback, p.get(), placeholders::_1);
    auto bound_sc_cb = bind(&Priv::mon_sc_callback, p.get(), placeholders::_1);
    thread szscmon_thrd(&subdaemon::SizeScaleMonitor::run, p->szscmon.get(), bound_sc_cb, bound_sz_cb);
    //join them (the stop method should actually stop these 2 instances from running, for this method to exit)
    if(ipcsvr_thrd.joinable()) {
        ipcsvr_thrd.join();
    }
    if(szscmon_thrd.joinable()) {
        szscmon_thrd.join();
    }

    p->running = false;
    p->destructor_waiter.interrupt();
}

void Daemon::stop() {
    if(p->running.load(memory_order_seq_cst) && !p->stopping.load(memory_order_seq_cst)) {
        p->stopping = true;
        p->server->stop();
        p->szscmon->stop();
    }
}

msg::MessageStore Daemon::Priv::server_callback(msg::MessageStore request) {
    //reply data
    msg::MessageStore repmsg;
    msgrep::ReplyHeader* rephdr = (msgrep::ReplyHeader*)repmsg.header.data;
    new (rephdr) msgrep::ReplyHeader(msgrep::ReplyType::NONE);

    msgreq::RequestHeader* reqhdr = (msgreq::RequestHeader*)request.header.data;
    switch (reqhdr->type) {
        case msgreq::RequestType::PING:
            rephdr->type = msgrep::ReplyType::PING_REPLY;
            break;
        case msgreq::RequestType::ASK_DISPLAY:
            nth->srvhdlr_ask_display(request, repmsg);
            break;
        case msgreq::RequestType::RELOAD_CFG:
            nth->srvhdlr_reload_cfg(repmsg);
            break;
        case msgreq::RequestType::NONE:
            LOG(INFO) << "Received a NONE request type.";
            break;
        default:
            LOG(INFO) << "Received an unsupported request type.";
            break;
    }
    return repmsg;
}

void Daemon::Priv::mon_sc_callback(uint16_t new_scfactor) {
    dpymgr->rescale(new_scfactor);
}

void Daemon::Priv::mon_sz_callback(pair<uint16_t, uint16_t> new_size_1x) {
    dpymgr->resize(new_size_1x);
}

void Daemon::NTHandler::srvhdlr_ask_display(msg::MessageStore &req, msg::MessageStore &rep) {
    //lambdas
    auto name_matcher = [&](int parent, string name) -> bool {
        //save some computation
        if(!name.size()) {
            return false;
        }
        static char pathfn_buff[4096];
        //full path match
        realpath(name.c_str(), pathfn_buff);
        if(p->cfg.abspath_match[parent].count(pathfn_buff)) {
            return true;
        }
        //name only
        strcpy(pathfn_buff, name.c_str());
        if(p->cfg.execname_match[parent].count(basename(pathfn_buff))) {
            return true;
        }
        return false;
    };
    struct PIDStat {
        pid_t ppid = 0;
        string cmdline;
        string exe;
    };
    auto get_pid_stat = [&](pid_t pid) -> PIDStat{
        static char procfs_buff[4096];
        PIDStat ret;
        //exename
        if(realpath(string_printf("/proc/%d/exe", pid).c_str(), procfs_buff)) {
            ret.exe = procfs_buff;
        }
        //cmdline
        auto fh = fopen(string_printf("/proc/%d/cmdline", pid).c_str(), "r");
        if(fh) {
            int amm_cmdline = fread(procfs_buff, 1, 4096, fh);
            fclose(fh);
            if(amm_cmdline) {
                //this will automatically copy only the argv[0], since cmdlines are null terminated for each argv
                ret.cmdline = procfs_buff;
            }
        }
        //ppid
        fh = fopen(string_printf("/proc/%d/status", pid).c_str(), "r");
        if(fh) {
            while(fgets(procfs_buff, 4096, fh)) {
                if(strncmp(procfs_buff, "PPid", 4) == 0) {
                    sscanf(procfs_buff, "PPid: %d\n", &ret.ppid);
                    break;
                }
            }
            fclose(fh);
        }
        //OK
        return ret;
    };

    //"return value"
    bool change = false;
    auto reqhdr = (msgreq::AskDisplayRequest*)req.header.data;
    if(reqhdr->requestor_type == msgreq::DisplayRequestorType::ONDEMAND) {
        //for ondemand, always enable scaling!
        change = true;
    } else if(reqhdr->requestor_type == msgreq::DisplayRequestorType::PRELOADER) {
        //we have PID here, we can match by both cmdline and exe
        pid_t pid = reqhdr->pid;
        for(int i = 0; i < p->cfg.execname_match.size(); ++i) {
            auto procstat = get_pid_stat(pid);
            //match both cmdline and exename
            if(name_matcher(i, procstat.cmdline) || name_matcher(i, procstat.exe)) {
                change = true;
                break;
            }
            //process parent next
            pid = procstat.ppid;
            if(!pid) {
                break;
            }
        }
    }

    //return
    auto rephdr = new (rep.header.data) msgrep::AskDisplayReply;
    rephdr->header.type = msgrep::ReplyType::DISPLAY_REPLY;
    if(change) {
        rephdr->change = 1;
        px::NullDelimiterBuilder bld(p->dpymgr->get_display_group_name());
        rep.payload.clear();
        rep.payload.insert(rep.payload.begin(), bld.size(), 0);
        memcpy(&rep.payload[0], bld.get_buffer(), bld.size());
    } else {
        rephdr->change = 0;
    }
}

void Daemon::NTHandler::srvhdlr_reload_cfg(msg::MessageStore &rep) {
    //reply
    auto rephdr = new (rep.header.data) msgrep::ReloadCfgReply();
    rephdr->success = 0;

    auto cfg = ConfigManager::from_file(p->cfg_file_src);
    if(!cfg.is_complete) {
        fputsn(_("Cannot reload configuration file."), stderr);
        LOG(ERROR) << "Reloading configuration failed.";
        return;
    }
    if(cfg.groups.size() != 1) {
        fputsn(_("Only 1 group configs are supported as of now."), stderr);
        return;
    } else {
        //success
        p->cfg = cfg;
        rephdr->success = 1;
    }
}