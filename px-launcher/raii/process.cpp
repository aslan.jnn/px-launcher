#include "process.h"
#include <sys/wait.h>
#include <glog/logging.h>

using namespace std;

class raii::Process::Priv {
public:
    pid_t pid = 0;
};

raii::Process::Process() {
    p = make_unique<Priv>();
}

raii::Process::Process(pid_t existing) {
    p = make_unique<Priv>();
    auto wait_res = waitpid(existing, nullptr, WNOHANG);
    if(wait_res == 0) {
        p->pid = existing;
    } else {
        if(wait_res < 0) {
            LOG(WARNING) << "Error waiting. Probably the process is not a child.";
        } else {
            LOG(INFO) << "Process exited before management.";
        }
    }
}

raii::Process::Process(vector<string> argv, vector<pair<string, string>> env) {
    p = make_unique<Priv>();
    //convert to array of char* (and nullptr terminated)
    vector<char*> argv_p;
    argv_p.insert(argv_p.begin(), argv.size() + 1, nullptr);
    for(int i=0; i<argv.size(); ++i) {
        argv_p[i] = (char*)argv[i].c_str();
    }
    pid_t ret = fork();
    if(ret) {
        //parent
        p->pid = ret;
    } else {
        //child
        setpgid(0, 0);
        fclose(stdin);
        fclose(stdout);
        fclose(stderr);
        for(auto& ienv : env) {
            setenv(ienv.first.c_str(), ienv.second.c_str(), 1);
        }
        execvp(argv_p[0], &argv_p[0]);
        //make sure failed execs doesn't stay around
        _exit(0);
    }
}

raii::Process::~Process() {
    //make sure this class isn't invalidated
    if(p->pid) {
        //make sure we check if the process has already exited before doing anything silly
        auto wait_res = waitpid(p->pid, nullptr, WNOHANG);
        if (wait_res == 0) {
            if (signal_on_exit) {
                kill(p->pid, signal_on_exit);
            }
            if (waitpid(p->pid, nullptr, WNOHANG) == 0 && sync_on_exit) {
                waitpid(p->pid, nullptr, 0);
            }
        }
        //else, either error (-1) or has already exited (PID). do nothing.
    }
}

void raii::Process::wait() {
    //make sure this class isn't invalidated
    if(p->pid) {
        auto wait_res = waitpid(p->pid, nullptr, WNOHANG);
        if (wait_res == 0) {
            waitpid(p->pid, nullptr, 0);
        }
        //whatever happened, invalidate this instance
        p->pid = 0;
    }
}