#include "sizescalemon.h"
#include <cstdlib>
#include <xcb/xcb.h>
#include <xcb/xcb_util.h>
#include <glog/logging.h>
#include "shared/memory.h"
#include <gdk/gdk.h>
#include <cmath>
#include "rdevtfd.h"
#include <atomic>

using namespace subdaemon;
using namespace std;
using namespace px::memory;

class SizeScaleMonitor::Priv {
public:
    px::xutil::XDisplayComponent fedpy{":0"};
    bool usable = false;
    xcb_connection_t* conn;
    xcb_screen_t* scr;
    GdkDisplay* gdkdpy;
    GdkScreen* gdkscr;

    atomic<bool> running {false};
    ReadEventFD loop_waiter;
    ReadEventFD destructor_waiter;

    uint16_t current_scfactor = 0;
    pair<uint16_t,uint16_t> current_size_1x {0,0};
    pair<uint16_t,uint16_t> current_size_orig {0,0};

    uint16_t retrieve_scfactor();
    pair<uint16_t,uint16_t> retrieve_size_orig();
    pair<uint16_t,uint16_t> calculate_size_1x(uint16_t w, uint16_t h);
};

SizeScaleMonitor::SizeScaleMonitor(px::xutil::XDisplayComponent fedpy) {
    p = make_unique<Priv>();
    p->fedpy = fedpy;
    //X11 core
    int screen_num;
    p->conn = xcb_connect(fedpy.to_string().c_str(), &screen_num);
    int conn_error = xcb_connection_has_error(p->conn);
    if(conn_error) {
        LOG(ERROR) << "X frontend connection error: " << conn_error;
        return;
    }
    auto svr_setup = xcb_get_setup(p->conn);
    xcb_screen_iterator_t screen_iter = xcb_setup_roots_iterator(svr_setup);
    for (int i = 0; i < screen_num; ++i) {
        xcb_screen_next(&screen_iter);
    }
    p->scr = screen_iter.data;
    //select STRUCTURE changes for root window
    uint32_t root_cw_event_mask = XCB_EVENT_MASK_STRUCTURE_NOTIFY;
    xcb_change_window_attributes(p->conn, p->scr->root, XCB_CW_EVENT_MASK, &root_cw_event_mask);

    //OK
    p->usable = true;
}

SizeScaleMonitor::~SizeScaleMonitor() {
    //stop any running threads first before clearing up resources
    if(p->running.load(memory_order_seq_cst)) {
        stop();
        p->destructor_waiter.wait();
    }

    xcb_disconnect(p->conn);
}

bool SizeScaleMonitor::is_usable() {
    return p->usable;
}

uint16_t SizeScaleMonitor::current_scfactor() {
    return (p->current_scfactor = p->retrieve_scfactor());
}

std::pair<uint16_t,uint16_t> SizeScaleMonitor::current_size_1x() {
    p->current_size_orig = p->retrieve_size_orig();
    return (p->current_size_1x = p->calculate_size_1x(p->current_size_1x.first, p->current_size_1x.second));
}

void SizeScaleMonitor::run(function<void(uint16_t)> rescale_callback, function<void(pair<uint16_t, uint16_t>)> resize_callback) {
    if(p->running.load(memory_order_seq_cst)) {
        return;
    }
    p->running = true;
    p->loop_waiter.clear();
    p->destructor_waiter.clear();

    //vars and lambdas
    uint16_t new_scfactor;
    auto check_scfactor = [&]() {
        new_scfactor = p->retrieve_scfactor();
        if(new_scfactor != p->current_scfactor) {
            p->current_scfactor = new_scfactor;
            rescale_callback(new_scfactor);
        }
    };
    pair<uint16_t,uint16_t> new_size_1x;
    auto set_size_1x = [&]() {
        if(new_size_1x != p->current_size_1x) {
            p->current_size_1x = new_size_1x;
            resize_callback(new_size_1x);
        }
    };

    //initial size
    p->current_size_orig = p->retrieve_size_orig();
    new_size_1x = p->calculate_size_1x(p->current_size_orig.first, p->current_size_orig.second);
    set_size_1x();

    //abuse wait vars
    fd_set fds;
    struct timespec wait_timeout {1, 0};

    //loop
    xcb_generic_event_t* evt;
    while (true) {
        //scaling factor
        check_scfactor();
        //adjust size 1x
        new_size_1x = p->calculate_size_1x(p->current_size_orig.first, p->current_size_orig.second);
        //check size_1x: process everything in the event queue, look for root's configure_notify
        while((evt = xcb_poll_for_event(p->conn))) {
            uint8_t evtcode = evt->response_type & 0x7F;
            if(evtcode == XCB_CONFIGURE_NOTIFY) {
                auto cevt = (xcb_configure_notify_event_t*)evt;
                if(cevt->window == p->scr->root) {
                    p->current_size_orig = {cevt->width, cevt->height};
                    new_size_1x = p->calculate_size_1x(cevt->width, cevt->height);
                }
            }
        }
        set_size_1x();
        //wait and check for exit
        FD_ZERO(&fds);
        FD_SET(p->loop_waiter.fd, &fds);
        if(pselect(p->loop_waiter.fd + 1, &fds, nullptr, nullptr, &wait_timeout, nullptr) > 0) {
            break;
        }
    }
    p->running = false;
    p->destructor_waiter.interrupt();
}

void SizeScaleMonitor::stop() {
    if(p->running.load(memory_order_seq_cst)) {
        p->loop_waiter.interrupt();
    }
}

uint16_t SizeScaleMonitor::Priv::retrieve_scfactor() {
    gdkdpy = gdk_display_open(fedpy.to_string().c_str());
    if(!gdkdpy) {
        LOG(ERROR) << "GDK3 display error.";
        return 1;
    }
    gdkscr = gdk_display_get_default_screen(gdkdpy);
    int pri_monitor = gdk_screen_get_primary_monitor(gdkscr);
    int scfactor = gdk_screen_get_monitor_scale_factor(gdkscr, pri_monitor);
    double dpi = gdk_screen_get_resolution(gdkscr);
    gdk_display_close(gdkdpy);
    //always prioritize scale factor
    if(scfactor > 1) {
        return scfactor;
    }
    //otherwise, the DE may not supports scaling. Let's derive from font's DPI instead.
    //X' DPI is not always reliable, so we should rely on this de-facto standard.
    constexpr double base_dpi = 96;
    return (uint16_t)max(1.0, round(dpi/base_dpi));
}

pair<uint16_t,uint16_t> SizeScaleMonitor::Priv::retrieve_size_orig() {
    //use previously retrieved scfactor
    if(!current_scfactor) {
        current_scfactor = retrieve_scfactor();
    }
    //core X
    UniquePointerWrapper_free <xcb_generic_error_t> err;
    auto root_geom = make_unique_malloc(xcb_get_geometry_reply(conn, xcb_get_geometry(conn, scr->root), &err.pop()));
    if(err.get()) {
        LOG(ERROR) << "Cannot retrieve root geometry: " << xcb_event_get_error_label(err.get()->error_code);
        return current_size_1x;
    }
    return {root_geom->width, root_geom->height};
}

pair<uint16_t,uint16_t> SizeScaleMonitor::Priv::calculate_size_1x(uint16_t w, uint16_t h) {
    //no rounding to 8, because our backend, Xvfb, supports arbitrary resolutions
    return {w / current_scfactor, h / current_scfactor};
}