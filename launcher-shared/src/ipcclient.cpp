#include "ipcclient.h"
#include "ipcmsg.h"
#include "ipcutil.h"
#include <string>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <random>
#include <chrono>
#include <errno.h>
#include <unistd.h>
#include <sys/select.h>
#include <unistd.h>
#include <ipcmsg.h>
#include "shared/util.h"

using namespace px::util;
using namespace ipcshared;
using namespace std;

class IPCClient::Priv {
public:
    string serveraddr;
    function<uint16_t(void)> rng;

    /**
     * Find an empty reply_id for use as daemon's reply socket channel.
     * @return 0 if none found, any number from 1 to 65535 if one successfully found.
     */
    uint16_t find_reply_id();
    /**
     *
     * @return A pair. First is the socket FD, second is reply ID. If first is -1, then reply socket creation was failed.
     */
    pair<int,uint16_t> create_reply_socket();
};

IPCClient::IPCClient(px::xutil::XDisplayComponent& server) {
    p = make_unique<Priv>();
    p->serveraddr = get_ipc_server_addr(server);

    //RNG for reply address generation
    default_random_engine generator;
    generator.seed(chrono::system_clock::now().time_since_epoch().count());
    uniform_int_distribution<uint16_t> distribution(2,32766);
    p->rng = bind(distribution, generator);
}

IPCClient::~IPCClient() {}

pair<IPCClient::ReplyStatus, msg::MessageStore> IPCClient::request_for_reply(msg::MessageStore req){
    msg::MessageStore rep;
    //default reply (in case of error)
    new (rep.header.data) msgrep::ReplyHeader(msgrep::ReplyType::NONE);

    //reply channel
    auto reply_ch = p->create_reply_socket();
    int reply_fd = reply_ch.first;
    uint16_t reply_id = reply_ch.second;
    if(reply_fd < 0) {
        return {ReplyStatus::REPLY_SOCKET_FAIL, rep};
    }
    //automatic destructor for reply channel
    DeleteInvoker reply_fd_deletor([&reply_fd, &reply_id](){
        close(reply_fd);
        unlink(get_ipc_reply_addr(reply_id).c_str());
    });

    //set appropriate fields in the request
    auto reqheader = ((msgreq::RequestHeader*)req.header.data);
    reqheader->reply_to = reply_id;
    reqheader->payload_sz = req.payload.size();

    //outgoing channel
    int req_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(req_fd < 0) {
        return {ReplyStatus::SEND_SOCKET_FAIL, rep};
    }
    DeleteInvoker req_fd_deletor([&req_fd](){
        close(req_fd);
    });
    //request/outgoing channel address
    sockaddr_un out_addr;
    out_addr.sun_family = AF_UNIX;
    strcpy(out_addr.sun_path, p->serveraddr.c_str());
    //send to daemon
    //header
    if(sendto(req_fd, reqheader, msg::MSGHDR_MAX_SIZE, 0, (sockaddr*)&out_addr, sizeof(sockaddr_un)) < 0) {
        return {ReplyStatus::SEND_SOCKET_FAIL, rep};
    }
    //payload
    if(reqheader->payload_sz) {
        if(sendto(req_fd, &req.payload[0], reqheader->payload_sz, 0, (sockaddr*)&out_addr, sizeof(sockaddr_un)) < 0) {
            return {ReplyStatus::SEND_SOCKET_FAIL, rep};
        }
    }

    constexpr timespec sel_timeout {2, 0};
    //first header, then payload (if exists)
    msgrep::ReplyHeader* reply_header;
    for(int i=0; i<2; ++i) {
        fd_set rd_set;
        FD_ZERO(&rd_set);
        FD_SET(reply_fd, &rd_set);
        //we don't care if the wait error
        if(pselect(reply_fd + 1, &rd_set, nullptr, nullptr, &sel_timeout, nullptr) == 0) {
            return {ReplyStatus::TIMED_OUT, rep};
        }

        //which one to read?
        size_t to_read;
        void* read_buff;
        if(i == 0) {
            to_read = msg::MSGHDR_MAX_SIZE;
            read_buff = rep.header.data;
        } else if (i == 1) {
            rep.payload.insert(rep.payload.begin(), reply_header->payload_sz, 0);
            to_read = reply_header->payload_sz;
            read_buff = &rep.payload[0];
        }
        //do the reading
        int actual_read;
        actual_read = read(reply_fd, read_buff, to_read);
        if(actual_read < 0) {
            return {ReplyStatus::RECV_FAIL, rep};
        } else if (actual_read < to_read) {
            return {ReplyStatus::DATA_TRUNCATED, rep};
        }
        //process the header
        if(i == 0) {
            reply_header = ((msgrep::ReplyHeader*)rep.header.data);
            if(reply_header->payload_sz == 0) {
                //no need to read zero payload
                break;
            }
        }
    }
    return {ReplyStatus::SUCCESS, rep};
}

uint16_t IPCClient::Priv::find_reply_id() {
    constexpr uint16_t max_retry = 64;
    struct stat st;
    //find an empty file to associate with
    for(int i=0; i<max_retry; ++i) {
        uint16_t candidate = rng();
        if(stat(get_ipc_reply_addr(candidate).c_str(), &st) < 0) {
            switch (errno) {
                case ENOENT:
                    //no file. found!
                    return candidate;
                case EACCES:
                    //"execute" access is denied from that dir. don't bother.
                    return 0;
            }
        }
        //no error means that the file is already used, continuing...
    }
    //we're run out of retries and hasn't found an ID. giving up ...
    return 0;
}

pair<int,uint16_t> IPCClient::Priv::create_reply_socket() {
    int socket_fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if(socket_fd < 0) {
        return {-1, 0};
    }

    uint16_t reply_id = find_reply_id();
    if(!reply_id) {
        return {-1, 0};
    }

    sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    string socketpath = get_ipc_reply_addr(reply_id);
    strcpy(addr.sun_path, socketpath.c_str());

    //bind
    int bind_stat = bind(socket_fd, (sockaddr*)&addr, sizeof(sockaddr_un));
    if (bind_stat < 0) {
        return {-1, 0};
    }
    //so that server can reply
    chmod(socketpath.c_str(), 0777);

    return {socket_fd, reply_id};
}