#pragma once
#include <sys/select.h>
#include <memory>

/**
 * Used to implement interruptable thread waiting, both standalone (similar in effect to condition variables) or in conjunction with other FD waiting.
 * Yes, we essentially abuse "select" to implement condition variable!
 */
class ReadEventFD {
public:
    const int fd;

    ReadEventFD();
    ~ReadEventFD();
    /**
     * Clear all interrupt.
     */
    void clear();
    /**
     * Makes this instance's FD selector stop waiting. Doesn't have any effect if the previous interrupt wasn't cleared.
     */
    void interrupt();
    /**
     * Select this FD only for indefinite time. Doesn't clear prior to or after waiting.
     */
    void wait();
    /**
     * Select this FD only for the specified timeout. Doesn't clear prior to or after waiting.
     */
    void wait(struct timespec* timeout);
};