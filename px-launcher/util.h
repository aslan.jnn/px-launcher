#pragma once

#include <string>
#include <xcb/xcb.h>

class Util {
public:
    static std::string get_home_directory();
    static std::string set_trail_slash(std::string path);
};

class StringUtil {
public:
    static void ltrim(std::string &s);
    static void rtrim(std::string &s);
    static void trim(std::string &s);
};